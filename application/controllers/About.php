<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index()
	{
		if (empty($this->session->userdata('username')) and $this->session->userdata('active')==0 )
		{
			redirect('Home');
		}
		$this->load->view('mosh/about');
	}
}
