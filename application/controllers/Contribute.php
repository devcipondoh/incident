<?php 

class Contribute extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata('username')) and $this->session->userdata('active')==0 )
		{
			redirect('Home');
		}
	}

	public function index()
	{
	    $cek_access = $this->db->get_where('tbl_sign_up',array('username'=>$this->session->userdata('username')))->row();
	    if($cek_access->subscribe==0)
	    {
	        $this->session->set_flashdata('msg','<script>alert("please subscribe to enter this page")</script>');
			redirect('Home');
	    }
	  // var_dump($cek_access);
	  // echo "$cek_access->subscribe";
		$this->load->view('mosh/contribute');
	}

	public function save_contribute()
	{
		$name_people = $this->db->escape_str($this->input->post('name_people'));
		$id_type = $this->db->escape_str($this->input->post('id_type'));
		$passport = $this->db->escape_str($this->input->post('passport'));
		$country_of_passport = $this->db->escape_str($this->input->post('country_of_passport'));
		$incident = $this->db->escape_str($this->input->post('incident'));
		$chronology = $this->db->escape_str($this->input->post('chronology'));
		$created_at = date('yyyy-mm-dd H:I:s');
		// $gambar = $_FILES['gambar']['name'];
		// print_r($gambar);die();
		// if($_FILES['gambar1']['name']==0 or $_FILES['gambar2']['name']==0 or $_FILES['gambar3']['name']==0)
		// {
		//     $this->session->set_flashdata('msg', '<script>alert("Please enter min 1 image")</script>');
		//     redirect('Contribute');
		// }
		$a = array();
		for ($i=1;$i<4;$i++)
		{
			$dok = 'gambar'.$i;
				$config['upload_path']          = './assets/gambar';
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size']             = 10000;
	   		$this->load->library('upload',$config);  
	   		$this->upload->initialize($config);
	    if ( ! $this->upload->do_upload($dok))
	    {
	            // $ket['dok'] = "";
	    }
	    else
	    {
	            $data = array('dok' => $this->upload->data());
	            $ket['dok'] = $data['dok']['file_name'];
	            // print_r($ket);die();
	            array_push($a,$ket);
	    }
		}
		// echo json_encode($a);die();

		$params = 
		array(
			'name_people' => $name_people,
			'id_type' => $id_type,
			'passport' => $passport,
			'country_of_passport' => $country_of_passport,
			'chronology' => $chronology,
			'incident' => $incident,
			'foto' => json_encode($a)
		);
		$this->db->insert('tbl_contribute', $params);
		$this->session->set_flashdata('msg', "Upload Berhasil");
		redirect('Contribute');
	}


	public function search()
	{
		$this->load->view('mosh/search');
	}

	public function result()
	{
		if ($this->input->post('detail')) {
			$name_people = $this->db->escape_str($this->input->post('name_people'));
			$incident = $this->db->escape_str($this->input->post('incident'));
			$passport = $this->db->escape_str($this->input->post('passport'));
			$country = $this->db->escape_str($this->input->post('country'));
			$params =
			array(
				'name_people' => $name_people,
				'incident' => $incident,
				'passport' => $passport,
				'country_of_passport' => $country
			);
			$data['result'] = $this->db->or_like($params)->get('tbl_contribute')->result();
			// print_r($data['result']);die();
			$this->session->set_flashdata('msg', 'result');
			$this->load->view('mosh/result',$data);
		}
		elseif($this->input->post('word'))
		{
			
			$name_people = $this->db->escape_str($this->input->post('textarea'));
			$incident = $this->db->escape_str($this->input->post('textarea'));
			$passport = $this->db->escape_str($this->input->post('textarea'));
			$country = $this->db->escape_str($this->input->post('textarea'));
			$params =
			array(
				'name_people' => $name_people,
				'incident' => $incident,
				'passport' => $passport,
				'country_of_passport' => $country
			);
			$data['result'] = $this->db->or_like($params)->get('tbl_contribute')->result();
			$this->session->set_flashdata('msg', '<script>alert("Successfully")</script>');
			$this->load->view('mosh/result',$data);
		}
		else
		{
			redirect('Contribute');
		}
	}
}