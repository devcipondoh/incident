<?php 

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata('username')) and $this->session->userdata('active')==0 )
		{
			redirect('Home');
		}
	}

	public function index()
	{
		$this->load->view('mosh/after_login');
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Page');
	}
}
?>