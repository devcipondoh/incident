<?php 

class Page extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!empty($this->session->userdata('username')) and $this->session->userdata('active')==1 )
		{
			redirect('Home');
		}
	}

	public function index()
	{
		$this->load->view('mosh/index.php');
	}

	public function sign_up()
	{
		$this->load->view('mosh/signup');
	}

	public function save_sign_up()
	{
		$username = $this->db->escape_str($this->input->post('username'));
		$password = $this->db->escape_str($this->input->post('password'));
		$re_password = $this->db->escape_str($this->input->post('re_password'));
		$first_name = $this->db->escape_str($this->input->post('first_name'));
		$surname = $this->db->escape_str($this->input->post('surname'));
		$company_name = $this->db->escape_str($this->input->post('company_name'));
		$posision = $this->db->escape_str($this->input->post('position'));
		$phone_number = $this->db->escape_str($this->input->post('phone_number'));
		$email = $this->db->escape_str($this->input->post('email'));
		$username = $this->db->escape_str($this->input->post('username'));
		$create_at = date('Y-m-d H:I:s');
		$subscribe = '0';
		$hash = rand(0,999999);
		$md5 = md5(rand(0,999999));

		if ($password == $re_password) 
		{
			$cek = $this->db->get_where('tbl_sign_up', array('username' => $username))->row();
			$cek2 = $this->db->get_where('tbl_sign_up', array('email' => $email))->row();

			if (count($cek) > 0) 
			{
				$this->session->set_flashdata('msg', "Username <b>'$username'</b> already.");
				redirect('Page/sign_up');
			}
			else if (count($cek2) > 0) 
			{
				$this->session->set_flashdata('msg', "E-mail <b>'$email'</b> already.");
				redirect('Page/sign_up');
			}
			else
			{
				$params = 
				array(
					'username' => $username,
					'password' => md5($password),
					'first_name' => $first_name,
					'surname' => $surname,
					'company_name' => $company_name,
					'posision' => $posision,
					'phone_number' => $phone_number,
					'email' => $email,
					'create_at' => $create_at,
					'subscribe' => $subscribe,
					'hash' => $hash.md5($surname),
					'md5' => $md5
				);

				stream_context_set_default([
					'ssl' =>
									[
										'verify_peer' => false,
										'verify_peer_name' => false,
		        'allow_self_signed' => true
									]	
				]);	
				// $this->load->library('encrypt');

				//SMTP & mail configuration
				$config = array(
				    'protocol'  => 'smtp',
				    'smtp_host' => 'ssl://smtp.googlemail.com',
				    'smtp_port' => 465,
				    'smtp_user' => 'agusxcode0101@gmail.com', //emailpengguna
				    'smtp_pass' => 'blackhatxcode', //passemailpengguna
		    // 'smtp_crypto' => 'security',
				    'mailtype'  => 'html',
				    'charset'   => 'utf-8'
				);
				$this->load->library('email',$config);
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");

				//Email content
				$htmlContent = '<h1>Activate Your Account</h1>';
				$htmlContent .= 'Please click this link to activate your account : <a href="'.base_url().'Page/activate_account/'.$md5.'/'.$hash.md5($surname).'/">link</a>';

				$this->email->to($email);
				$this->email->from('agusxcode0101@gmail.com','Activate Account'); //emailpengguna
				$this->email->subject('Activate Account');
				$this->email->message($htmlContent);

				//Send email
				$this->email->send();
				$this->db->insert('tbl_sign_up', $params);
				$this->session->set_flashdata('msg', "Sign up is successfully, Please Check Your E-mail");
				redirect('Page/login');
			}
		}
		else
		{
			$this->session->set_flashdata('msg', "Ooppss... Your password and re-password not matching.");
			redirect('Page/sign_up');
		}
		
	}

	public function login()
	{
		$this->load->view('mosh/login');
	}

	public function check_login()
	{
		$username = $this->db->escape_str($this->input->post('username'));
		$password = $this->db->escape_str($this->input->post('password'));
		$cek = $this->db->get_where('tbl_sign_up',array('username'=>$username,'password'=> md5($password)))->row();
		if(empty($cek))
		{
			$this->session->set_flashdata('msg', "Login failed, please check username and password");
			redirect('Page/login');
		}
		else if($cek->activate==1)
		{
			$data1 = array(
				'username'=>$username,
				'email'=>$cek->email,
				'no_hp'=>$cek->phone_number,
				'firstname'=>$cek->first_name,
				'active'=>$cek->activate,
				'surname'=>$cek->surname
			);
			$this->session->set_userdata($data1);
			redirect('Home');
		}
		else
		{
			$this->session->set_flashdata('msg', "Login failed, please activate your account");
			redirect('Page/login');
		}
	}

	public function activate_account($md5,$hash)
	{
		$cek = $this->db->update('tbl_sign_up',array('activate'=>1,'hash'=>''),array('hash'=>$hash,'md5'=>$md5));
		if($cek = false)
		{
			$this->session->set_flashdata('msg', "Sorry, Your account is active");
		}
		else
		{
			$this->session->set_flashdata('msg', "Activate Success");
		}
		redirect('Page/login');
	}

	public function view_forget_password()
	{
		$this->load->view('mosh/forget_password');
	}

	public function forget_password()
	{
		$email = $this->db->escape_str($this->input->post('email'));
		$password = $this->db->escape_str($this->input->post('password'));
		$re_password = $this->db->escape_str($this->input->post('re_password'));
		$hash = rand(0,999);
		$hash_ = rand(0,999);
		if ($password == $re_password) 
		{
		// print_r($password.'100'.$re_password);die();
			$cek = $this->db->update('tbl_sign_up',array('hash'=>'Lo1'.$hash.'re2'.$hash_.'m3'.md5($password)),array('email'=>$email));
			if($cek == true)
			{
						$config = array(
						    'protocol'  => 'smtp',
						    'smtp_host' => 'ssl://smtp.googlemail.com',
						    'smtp_port' => 465,
						    'smtp_user' => 'xxx@gmail.com', //emailpengguna
						    'smtp_pass' => 'xxx', //passemailpengguna
				    // 'smtp_crypto' => 'security',
						    'mailtype'  => 'html',
						    'charset'   => 'utf-8'
						);
						$this->load->library('email',$config);
						$this->email->initialize($config);
						$this->email->set_mailtype("html");
						$this->email->set_newline("\r\n");

						//Email content
						$htmlContent = '<h1>Repair Your Password</h1>';
						$htmlContent .= 'Please click this link to confirm your new password : <a href="'.base_url().'Page/confirm_password/Lo1'.$hash.'re2'.$hash_.'m3'.md5($password).'/'.md5($password).'">link</a>';

						$this->email->to($email);
						$this->email->from('xxx@gmail.com','Forget Password'); //emailpengguna
						$this->email->subject('Activate Account');
						$this->email->message($htmlContent);

						//Send email
						$this->email->send();
						$this->session->set_flashdata('msg', "Please check your e-mail to confirm your new password");
						redirect('Page/login');
			}
			else
			{
				$this->session->set_flashdata('msg', "Your email not valid");
				redirect('Page/view_forget_password');
			}
		}
		else
		{
			$this->session->set_flashdata('msg', "Ooppss... Your password and re-password not matching.");
			redirect('Page/view_forget_password');
		}
	}

	public function confirm_password($hash,$pass)
	{
		$cek = $this->db->update('tbl_sign_up',array('password'=>$pass),array('hash'=>$hash));
		// print_r($cek);die();
		if($cek == true)
		{
			$this->session->set_flashdata('msg', "Confirm password success");
			redirect('Page/login');
		}
		else
		{
			$this->session->set_flashdata('msg', "The link has expired");
			redirect('Page/forget_password');
		}
	}
	
}