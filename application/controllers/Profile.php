<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		if (empty($this->session->userdata('username')) and $this->session->userdata('active')==0 )
		{
			redirect('Home');
		}
		$data['profil'] =$this->db->get_where('tbl_sign_up',array('username'=>$this->session->userdata('username')))->row();
		$this->load->view('mosh/profile',$data);
	}
}
