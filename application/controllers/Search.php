<?php 

class Search extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata('username')) and $this->session->userdata('active')==0 )
		{
			redirect('Home');
		}
	}

	public function index()
	{
	    $cek_access = $this->db->get_where('tbl_sign_up',array('username'=>$this->session->userdata('username')))->row();
	    if($cek_access->subscribe==0)
	    {
	        $this->session->set_flashdata('msg','<script>alert("please subscribe to enter this page")</script>');
			redirect('Home');
	    }
		$this->load->view('mosh/search');
	}

	public function result()
	{
		if ($this->input->post('detail')) {
			$name_people = $this->db->escape_str($this->input->post('name_people'));
			$incident = $this->db->escape_str($this->input->post('incident'));
			$passport = $this->db->escape_str($this->input->post('passport'));
			$country = $this->db->escape_str($this->input->post('country'));
			if(empty($name_people) AND empty($incident) AND empty($passport) AND empty($country))
			{
			    $this->session->set_flashdata('msg', '<script>alert("Please fill textbox")</script>');
			    redirect('Search');
			}
			$params =
			array(
				'name_people' => $name_people,
				'incident' => $incident,
				'passport' => $passport,
				'country_of_passport' => $country
			);
			$data['result'] = $this->db->like($params)->get('tbl_contribute')->result();
			// print_r($data['result']);die();
			$this->load->view('mosh/result',$data);
		}
		elseif($this->input->post('word'))
		{
			$name_people = $this->db->escape_str($this->input->post('textarea'));
			$incident = $this->db->escape_str($this->input->post('textarea'));
			$passport = $this->db->escape_str($this->input->post('textarea'));
			$country = $this->db->escape_str($this->input->post('textarea'));
			$chronology = $this->db->escape_str($this->input->post('textarea'));
			$params =
			array(
				'name_people' => $name_people,
				'incident' => $incident,
				'passport' => $passport,
				'country_of_passport' => $country,
				'chronology' => $chronology
			);
			$data['result'] = $this->db->or_like($params)->get('tbl_contribute')->result();
			$this->load->view('mosh/result',$data);
		}
		else
		{
			redirect('Contribute');
		}
	}
}