<?php 

class Contribute extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		$this->load->view('mosh/contribute');
	}

	public function save_contribute()
	{
		$name_people = $this->db->escape_str($this->input->post('name_people'));
		$id_type = $this->db->escape_str($this->input->post('id_type'));
		$passport = $this->db->escape_str($this->input->post('passport'));
		$country_of_passport = $this->db->escape_str($this->input->post('country_of_passport'));
		$incident = $this->db->escape_str($this->input->post('incident'));
		$chronology = $this->db->escape_str($this->input->post('chronology'));
		$created_at = date('yyyy-mm-dd H:I:s');
		// $gambar = $_FILES['gambar']['name'];
		// print_r($gambar);die();
		$a = array();
		for ($i=1;$i<4;$i++)
		{
			$dok = 'gambar'.$i;
				$config['upload_path']          = './assets/gambar';
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size']             = 10000;
	   		$this->load->library('upload',$config);  
	   		$this->upload->initialize($config);
	    if ( ! $this->upload->do_upload($dok))
	    {
	            // $ket['dok'] = "";
	    }
	    else
	    {
	            $data = array('dok' => $this->upload->data());
	            $ket['dok'] = $data['dok']['file_name'];
	            // print_r($ket);die();
	            array_push($a,$ket);
	    }
		}
		// echo json_encode($a);die();

		$params = 
		array(
			'name_people' => $name_people,
			'id_type' => $id_type,
			'passport' => $passport,
			'country_of_passport' => $country_of_passport,
			'chronology' => $chronology,
			'incident' => $incident,
			'foto' => json_encode($a)
		);
		$this->db->insert('tbl_contribute', $params);
		$this->session->set_flashdata('msg', "Upload Berhasil");
		redirect('Page/login');
	}
}