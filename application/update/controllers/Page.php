<?php 

class Page extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		$this->load->view('mosh/login');
	}

	public function sign_up()
	{
		$this->load->view('mosh/signup');
	}

	public function login()
	{
		$username = $this->db->escape_str($this->input->post('username'));
		$password = $this->db->escape_str($this->input->post('password'));
		$cek = $this->db->get_where('tbl_sign_up',array('username'=>$username,'password'=>$password))->row();
		if(!empty($cek))
		{
			$data1 = array(
				'username'=>$username,
				'email'=>$cek->email,
				'no_hp'=>$cek->phone_number,
				'firstname'=>$cek->first_name,
				'surname'=>$cek->surname
			);
			$this->session->set_userdata($data1);
			redirect('Page');
		}
		else
		{
			redirect('Page');
		}
	}

	public function save_sign_up()
	{
		$username = $this->db->escape_str($this->input->post('username'));
		$password = $this->db->escape_str($this->input->post('password'));
		$re_password = $this->db->escape_str($this->input->post('re_password'));
		$first_name = $this->db->escape_str($this->input->post('first_name'));
		$surname = $this->db->escape_str($this->input->post('surname'));
		$company_name = $this->db->escape_str($this->input->post('company_name'));
		$posision = $this->db->escape_str($this->input->post('posision'));
		$phone_number = $this->db->escape_str($this->input->post('phone_number'));
		$email = $this->db->escape_str($this->input->post('email'));
		$username = $this->db->escape_str($this->input->post('username'));
		$create_at = date('yyyy-mm-dd H:I:s');
		$subscribe = '0';

		$params = 
		array(
			'username' => $username,
			'password' => md5($username),
			'first_name' => $first_name,
			'surname' => $surname,
			'company_name' => $company_name,
			'posision' => $posision,
			'phone_number' => $phone_number,
			'email' => $email,
			'create_at' => $create_at,
			'subscribe' => $subscribe
		);
		$cek = $this->get_where('tbl_sign_up', array('username' => $username))->row();

		if ($cek->num_rows > 0) 
		{
			$this->session->set_flashdata('msg', "This is username <b>'$username'</b> already.");
			redirect('Page/sign_up');
		}
		else
		{
			$this->db->insert('tbl_sign_up', $params);
			$this->session->set_flashdata('msg', "Sign up is successfully.");
			redirect('Page/login');
		}
	}
}