-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 21 Nov 2018 pada 17.17
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_projek`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_contribute`
--

CREATE TABLE IF NOT EXISTS `tbl_contribute` (
`id_contribute` int(11) NOT NULL,
  `name_people` text NOT NULL,
  `id_type` varchar(255) NOT NULL,
  `passport` varchar(255) NOT NULL,
  `country_of_passport` varchar(255) NOT NULL,
  `chronology` text NOT NULL,
  `incident` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_contribute`
--

INSERT INTO `tbl_contribute` (`id_contribute`, `name_people`, `id_type`, `passport`, `country_of_passport`, `chronology`, `incident`, `foto`, `created_at`) VALUES
(1, 'fdsjnkj', '8', 'khkj', 'jkbjk', '', 'kmbjkb', '[{"dok":"kuesioner2.jpg"},{"dok":"profil4.png"}]', '2018-11-21 23:12:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_contribute`
--
ALTER TABLE `tbl_contribute`
 ADD PRIMARY KEY (`id_contribute`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_contribute`
--
ALTER TABLE `tbl_contribute`
MODIFY `id_contribute` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
