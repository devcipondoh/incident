<!DOCTYPE html>
<html>
<head>
	<title>Contribute</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-image: url('bg_4.jpg'); background-size: cover">
	<div class="content">
		<div class="col-md-12" style="background-color: white;margin-top: -10px;padding: 10px">
			<div class="pull-left" style=";margin-left: 10%">
				<a href="search.html" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">search.</a>&emsp;&emsp;
				<a style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200"><u><b>contribute.</b></u></a>&emsp;&emsp;
				<a style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">about.</a>&emsp;&emsp;
				<a style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">profile.</a>&emsp;&emsp;
			</div>
			<div class="pull-right" style="margin-right: 10%">
				<button class="btn btn-xs" style=";margin-top: 5px;border-radius: 50px;color: white;background-color: black;font-size: 18px">&emsp;&emsp;&emsp;Join now&emsp;&emsp;&emsp;</button>
			</div>
		</div>
		<br><br><br><br>
		<div class="col-md-12">
		  <div class="col-md-1">
            <h1 style="transform: rotate(-90deg);font-weight: 800;font-size: 75px;opacity: 0.5;margin-top: 350px;margin-left: 20%;position: relative;"> Contribute.</h1>
		  </div>
		  <form action="<?=base_url()?>Contribute/save_contribute" method="post" enctype="multipart/form-data">
		  <div class="col-md-5">
		    <h2 style="font-family: arial;font-weight: 900;color: white">&nbsp; Share what you know.</h2><br>
		    <center>
	    		<div class="col-md-10">
	    			<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="name_people" placeholder="Name of People"><br>
	    		</div>
		    	<div class="col-md-10">
		    		<input type="number" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="id_type" placeholder="ID Type"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="passport" placeholder="Passport or ID Card Number"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="country_of_passport" placeholder="Country of Passport or ID Card"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="incident" placeholder="Type of Incident"><br>
		    	</div></center>
		    </div>
		  <div class="col-md-5">
		  	<br><br><br><br>
		  	<textarea class="form-control col-md-12" name="chronology" style="height: 120px;font-weight: 600;text-align: center;">Details / Chronology</textarea>
		  	<div class="col-md-12"><br></div>
		  	<div class="col-md-12">
			  	<input style="display: none" type="file" id="preview_gambar1" name="gambar1">
			  	<img src="<?=base_url()?>assets/gambar/plus.png" id="gambar_nodin1" class="col-md-3" style=";height: 140px;border:5px dashed silver;margin-right: 40px;cursor: pointer">
			  	<input style="display: none" type="file" id="preview_gambar2" name="gambar2">
			  	<img src="<?=base_url()?>assets/gambar/plus.png"  id="gambar_nodin2" class="col-md-3" style=";height: 140px;border:5px dashed silver;margin-right: 40px;cursor: pointer">
			  	<input style="display: none" type="file" id="preview_gambar3" name="gambar3">
			  	<img src="<?=base_url()?>assets/gambar/plus.png"  id="gambar_nodin3" class="col-md-3" style=";height: 140px;border:5px dashed silver;margin-right: 40px;cursor: pointer">
		  	</div>
		  </div>
		  <center>
	    	<div class="col-md-12">
	    		<button type="submit" class="btn btn-warning" style="color: white;background-color: black;font-size: 19px;margin-left:-6%;margin-top: -100px;z-index: 999999">&emsp;&emsp;Submit&emsp;&emsp;</button>
	    	</div>
		   </center>
		  </form>
		   <div class="col-md-12"></div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
	function bacaGambar(input,gambar) {
   if (input.files && input.files[0]) {
      var reader = new FileReader();
 
      reader.onload = function (e) {
          $('#'+gambar).attr('src', e.target.result);
      }
 
      reader.readAsDataURL(input.files[0]);
   }
}
$("#preview_gambar1").change(function(){
   bacaGambar(this,'gambar_nodin1');
});
$("#preview_gambar2").change(function(){
   bacaGambar(this,'gambar_nodin2');
});
$("#preview_gambar3").change(function(){
   bacaGambar(this,'gambar_nodin3');
});
$(document).ready(function()
{
	$('#gambar_nodin1').on('click',function(){
		$('#preview_gambar1').click();
	})
	$('#gambar_nodin2').on('click',function(){
		$('#preview_gambar2').click();
	})
	$('#gambar_nodin3').on('click',function(){
		$('#preview_gambar3').click();
	})
})
</script>