<!DOCTYPE html>
<html>
<head>
  <title>Landing Page</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="font-family: TIMES NEW ROMAN;color: white">
    <div class="content">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active" style="background-image: url('<?=base_url()?>assets/gambar/bg_1.jpg'); background-size: 100% 100% ">
              <h1 style="font-size: 75px; margin-top: 50px; margin-left: 100px;color: white" class="pull-left"><b>Persona <br> Non Garata</b></h1>
              <a href="login.html"><button class="btn btn-xs pull-right" style="color:black;padding: 0px; margin-top: 30px;margin-right: 100px; font-size: 30px; border-radius: 15px">&nbsp;&nbsp;Log in. &nbsp; </button></a>
              <a href="signup.html"><button class="btn btn-xs pull-right" style="color:black;padding: 0px; margin-top: 30px;margin-right: 20px; font-size: 30px; border-radius: 15px">&nbsp;&nbsp;Sign up &nbsp; </button></a>
              <img src="pubg.jpg" style="opacity: -9999999999">
            </div>

            <div class="item" style="background-image: url('<?=base_url()?>assets/gambar/bg_2.jpg'); background-size: 100% 100% ">
              <button class="btn btn-xs pull-right" style="color:black;padding: 0px; margin-top: 30px;margin-right: 100px; font-size: 30px; border-radius: 15px">&nbsp;&nbsp;Log in. &nbsp; </button>
              <button class="btn btn-xs pull-right" style="color:black;padding: 0px; margin-top: 30px;margin-right: 20px; font-size: 30px; border-radius: 15px">&nbsp;&nbsp;Sign up &nbsp; </button>
              <div class="col-md-7" style="position: absolute;margin-top: 25%;margin-left: 100px;z-index: 9999">
                  <h5 style="font-size: 75px;"><b>Want to Share or Find Out<br>About Problem Guests?</b></h5>
                  <h2><b>Sign Up and Join Perth c own Guests<br>Database Registry of Problem Guests</b></h2>
                  <button class="btn btn-xs" style="color:black;position:relative;font-size: 30px; border-radius: 15px;">&emsp;&emsp;&emsp;Sign Up&emsp;&emsp;&emsp;</button>
              </div>
              <img src="pubg.jpg" style="opacity: -9999999999">
            </div>
          </div>
        </div>
    </div>
</body>
</html>