<!DOCTYPE html>
<html>
<head>
	<title>Log in</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="color: black;font-family: arial">
			<h1 style="opacity: 0.1;font-family: times new roman; font-size: 90px;position: absolute;transform: rotate(270deg); margin-top: 150px">Login.</h1>
	<div class="content" style="margin: 15%">
		<center>
		<div class="panel" style="width: 50%">
		  <div class="panel-body" style="box-shadow: 2px 2px 2px grey;">
		    <h2 style="font-family: times new roman;font-weight: 900">log in.</h2><br>
		    <form style="margin-left: 15%;">
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="username" placeholder="Username"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="password" placeholder="Password"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<button class="btn btn-warning" style="color: white;background-color: black;font-size: 19px">&emsp;&emsp;<b>Submit</b>&emsp;&emsp;</button><br><h5 style="font-weight: 900">Forget Password?</h5><br><br>
		    	</div>
		    </form>
		  </div>
		</div>
	</center>
	</div>
</body>
</html>