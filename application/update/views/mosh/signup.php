<!DOCTYPE html>
<html>
<head>
	<title>Sign up</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="color: black;font-family: arial">
	<h1 style="opacity: 0.1;font-family: times new roman; font-size: 90px;position: absolute;transform: rotate(270deg); margin-top: 250px;margin-left: -100px">Sign up.</h1>
	<div class="content" style="margin: 5%">
		<center>
		<div class="panel" style="width: 80%">
		  <div class="panel-body" style="box-shadow: 2px 2px 2px grey;">
		    <h2 style="font-family: times new roman;font-weight: 900">sign up.</h2><br>
		    <h2 class="text-danger"><?php $this->ses?></h2>
		    <form style="margin-left: 15%;" method="POST" action="<?=base_url('Page/save_sign_up')?>">
	    		<div class="col-md-5">
	    			<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="first_name" placeholder="First Name">
	    		</div>
	    		<div class="col-md-5">
	    			<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" placeholder="Surname" name="surname"><br>
	    		</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="company_name" placeholder="Company Name"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="position" placeholder="Position"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="phone_number" placeholder="Phone Number"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="email" placeholder="Email"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="username" placeholder="Username"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="password" placeholder="Password"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="re_password" placeholder="Re-Password"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<button class="btn btn-warning" style="color: white;background-color: black;font-size: 19px">&emsp;&emsp;Submit&emsp;&emsp;</button><br><br><br>
		    	</div>
		    </form>
		  </div>
		</div>
	</center>
	</div>
</body>
</html>