<!DOCTYPE html>
<html>
<head>
	<title>About.</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-image: url('<?=base_url()?>assets/gambar/bg_8.jpg');background-size: cover">
	<div class="content">
    <div class="col-md-12" style="position: absolute;z-index: 99999;margin-top:0;background-color: transparent;padding: 10px">
      <div class="container">
        <div class="pull-left">
          <a href="<?=base_url()?>Search" style="color: white;font-size: 30px;font-family: times new roman;font-weight: 200">search.</a>&emsp;&emsp;
          <a href="<?=base_url()?>Contribute/" style="color: white;font-size: 30px;font-family: times new roman;font-weight: 200">contribute.</a>&emsp;&emsp;
          <a href="<?=base_url()?>About/" style="color: white;font-size: 30px;font-family: times new roman;font-weight: 200"><u><b>about.</b></u></a>&emsp;&emsp;
          <a href="<?=base_url()?>Profile/" style="color: white;font-size: 30px;font-family: times new roman;font-weight: 200">profile.</a>&emsp;&emsp;
        </div>
        <div class="pull-right">
          <a href="<?=base_url()?>Paypal/"><button class="btn btn-xs" style=";margin-top: 5px;border-radius: 50px;color: white;background-color: black;font-size: 18px">&emsp;&emsp;&emsp;Join now&emsp;&emsp;&emsp;</button>
        </div></a>
      </div>
    </div>
		<div class="col-md-7" style="">
				<h1 style="color: white;opacity: 0.2;font-family: times new roman; font-size: 90px;position: absolute;transform: rotate(-90deg); margin-top: 350px;margin-left: -9%"><b>About.</b></h1>
		</div>
		<div class="col-md-5">
		</div>
			<div class="col-md-7" style="background:transparent;position: relative;"></div>
			<div class="col-md-5" style="background:white;position: relative;background-size: 100%;height: 50em !important">	
			</div>
				
		<div class="panel" style="position: absolute;width: 87%;margin-top: 8%;margin-left: 10%; box-shadow: 1px 0px 1px 1px silver">
			<div class="panel panel-body">
				<div class="col-md-7">
					<img src="<?=base_url()?>assets/gambar/bg_7.jpg" width="100%">
				</div>
				<div class="col-md-5">
					 	<h1>Company</h1>
						<b>What is a 'Company'??</b><br><br>

						<p align="justify">&emsp;A company is a legal entity formed by a group of individuals to engage 
						in and operate a business enterprise. A company may be organized in various 
						ways for tax and financial liability purposes depending on the corporate law of it
						s jurisdiction. The line of business the company is in will generally
						 determine which business structure it chooses, for example a partnership, a
						  proprietorship, or a corporation. As such, a company may be regarded as a business type.</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>