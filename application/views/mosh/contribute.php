<!DOCTYPE html>
<html>
<head>
	<title>Contribute</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-image: url('<?=base_url()?>assets/gambar/bg_9.jpg'); background-size: cover">
	<div class="content">
    <div class="col-md-12" style="position: absolute;z-index: 99999;margin-top: 0;background-color: white;padding: 10px">
      <div class="container">
        <div class="pull-left">
          <a href="<?=base_url()?>Search" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">search.</a>&emsp;&emsp;
          <a href="<?=base_url()?>Contribute/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200"><u><b>contribute.</b></u></a>&emsp;&emsp;
          <a href="<?=base_url()?>About/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">about.</a>&emsp;&emsp;
          <a href="<?=base_url()?>Profile/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">profile.</a>&emsp;&emsp;
        </div>
        <div class="pull-right">
          <a href="<?=base_url()?>Paypal/"><button class="btn btn-xs" style=";margin-top: 5px;border-radius: 50px;color: white;background-color: black;font-size: 18px">&emsp;&emsp;&emsp;Join now&emsp;&emsp;&emsp;</button></a>
        </div>
      </div>
    </div>
		<div class="col-md-12" style="position: relative;">
		<br><br><br><br>
		  <div class="col-md-1">
            <h1 style="color:white;transform: rotate(-90deg);font-weight: 800;font-size: 75px;opacity: 0.5;margin-top: 420px;margin-left:-50px;position: relative;"> Contribute.</h1>
		  </div>
		  <form action="<?=base_url()?>Contribute/save_contribute" method="post" enctype="multipart/form-data">
		  <div class="col-md-5">
		    <h2 style="font-family: arial;font-weight: 900;color: white">&nbsp; Share what you know.</h2><br>
		    <center>
	    		<div class="col-md-10">
	    			<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="name_people" placeholder="Name of People" required=""><br>
	    		</div>
		    	<div class="col-md-10">
		    		<select class="form-control" name="id_type" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px">
			    		<?php
			    		$id_type = $this->db->get('id_type')->result();
			    		foreach ($id_type as $key => $it) {
			    			echo '<option value="'.$it->nama_id_type.'">'.$it->nama_id_type.'</option>';
			    		}?>
			    	</select><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="passport" placeholder="Passport or ID Card Number" required=""><br>
		    	</div>
		    	<div class="col-md-10">
		    		<select class="form-control" name="country_of_passport" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px">
		    			<option value="">Country of Passport or ID Card</option>
			    		<?php
			    		$country = $this->db->get('negara')->result();
			    		foreach ($country as $key => $it) {
			    			echo '<option value="'.$it->nama_negara.'">'.$it->nama_negara.'</option>';
			    		}?>
			    	</select><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="incident" placeholder="Type of Incident" required=""><br>
		    	</div></center>
		    </div>
		  <div class="col-md-5">
		  	<br><br><br><br>
		  	<textarea class="form-control col-md-12" name="chronology" style="height: 120px;font-weight: 600;text-align: center;" placeholder="Details / Chronology" required=""></textarea>
		  	<div class="col-md-12"><br></div>
		  	<div class="col-md-12">
			  	<input style="display: none" type="file" id="preview_gambar1" name="gambar1">
			  	<img src="<?=base_url()?>assets/gambar/plus.png" id="gambar_nodin1" class="col-md-3" style=";height: 140px;border:5px dashed silver;margin-right: 40px;cursor: pointer">
			  	<input style="display: none" type="file" id="preview_gambar2" name="gambar2">
			  	<img src="<?=base_url()?>assets/gambar/plus.png"  id="gambar_nodin2" class="col-md-3" style=";height: 140px;border:5px dashed silver;margin-right: 40px;cursor: pointer">
			  	<input style="display: none" type="file" id="preview_gambar3" name="gambar3">
			  	<img src="<?=base_url()?>assets/gambar/plus.png"  id="gambar_nodin3" class="col-md-3" style=";height: 140px;border:5px dashed silver;margin-right: 40px;cursor: pointer">
		  	</div>
		  </div>
		  <center>
	    	<div class="col-md-12">
	    		<button type="button" id="tambah_data" class="btn" style="color: black;background-color: lightgrey;font-size: 19px;margin-left:-6%;margin-top: -150px;z-index: 999999">&emsp;&emsp;Submit&emsp;&emsp;</button>
	    	</div>
		   </center>
		  </form>
		   <div class="col-md-12"></div>
	</div>
</div>
    <?=$this->session->flashdata('msg')?>
</body>
</html>
<script type="text/javascript">
	function bacaGambar(input,gambar) {
   if (input.files && input.files[0]) {
      var reader = new FileReader();
 
      reader.onload = function (e) {
          $('#'+gambar).attr('src', e.target.result);
      }
 
      reader.readAsDataURL(input.files[0]);
   }
}
$("#preview_gambar1").change(function(){
   bacaGambar(this,'gambar_nodin1');
});
$("#preview_gambar2").change(function(){
   bacaGambar(this,'gambar_nodin2');
});
$("#preview_gambar3").change(function(){
   bacaGambar(this,'gambar_nodin3');
});
$(document).ready(function()
{
	$('#gambar_nodin1').on('click',function(){
		$('#preview_gambar1').click();
	})
	$('#gambar_nodin2').on('click',function(){
		$('#preview_gambar2').click();
	})
	$('#gambar_nodin3').on('click',function(){
		$('#preview_gambar3').click();
	})
	$('#tambah_data').on('click',function()
	{
		var gambar1 = $('[name="gambar1"]').val();
		var gambar2 = $('[name="gambar2"]').val();
		var gambar3 = $('[name="gambar3"]').val();
		if(gambar1==0 && gambar2==0 && gambar3==0)
		{
			alert('please insert min 1 image')
		}
		else
		{
			$(this).attr('type','submit');
			$(this).click();
		}
	})
})
</script>