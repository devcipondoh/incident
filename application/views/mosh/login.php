<!DOCTYPE html>
<html>
<head>
	<title>Log in</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="color: black;font-family: arial;opacity: 1">
	<a href="<?=base_url()?>Page" style="color:black;position: absolute;margin-top: -5em;padding: 10px;font-size: 25px;opacity: 0.8;text-decoration: none"><i class="glyphicon glyphicon-arrow-left"></i>&emsp;<b>Back</b></a>
	<h1 style="opacity: 0.1;font-family: times new roman; font-size: 80px;position: absolute;transform: rotate(270deg); margin-top: 250px;margin-left: -50px">Login.</h1>
	<div class="content" style="margin-top: 10em">
		<center>
		 <h3 class="text-danger"><?=$this->session->flashdata('msg')?></h3>
		<div class="panel" style="width: 35%">
		  <div class="panel-body" style="box-shadow: 0px 1px 3px 1px grey;">
		    <h2 style="font-family: times new roman;font-weight: 900">log in.</h2><br>
		    <form style="margin-left: 15%;" action="<?=base_url()?>Page/check_login" method="post">
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="username" placeholder="Username"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="password" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="password" placeholder="Password"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<button class="btn btn-warning" style="color: white;background-color: black;font-size: 19px">&emsp;&emsp;<b>Submit</b>&emsp;&emsp;</button><br><a href="<?=base_url()?>Page/view_forget_password"><h5 style="font-weight: 900;color:black">Forget Password ?</h5></a><br><br>
		    	</div>
		    </form>
		  </div>
		</div>
	</center>
	</div>
</body>
</html>