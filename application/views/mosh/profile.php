<!DOCTYPE html>
<html>
<head>
  <title>Landing Page</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="font-family: TIMES NEW ROMAN;color: white;background-image: url('<?=base_url()?>assets/gambar/bg_5.jpg'); background-size: 100%;opacity: 0.9">
    <div class="col-md-12" style="position: absolute;z-index: 99999">
      <div class="container" style="margin-top: 10px;margin-bottom: 10px">
        <div class="pull-left">
          <a href="<?=base_url()?>Search" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">search.</a>&emsp;&emsp;
          <a href="<?=base_url()?>Contribute/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">contribute.</a>&emsp;&emsp;
          <a href="<?=base_url()?>About/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">about.</a>&emsp;&emsp;
          <a href="<?=base_url()?>Profile/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200"><u><b>profile.</b></u></a>&emsp;&emsp;
        </div>
        <div class="pull-right">
          <a href="<?=base_url()?>Paypal/"><button class="btn btn-xs" style=";margin-top: 5px;border-radius: 50px;color: white;background-color: black;font-size: 18px">&emsp;&emsp;&emsp;Join now&emsp;&emsp;&emsp;</button></a>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="col-md-2 pull-left" style="background-color: white;height: 800px;margin-left: -20px">
      </div>
      <div class="col-md-4">
        <div class="panel" style="width: 75%;color: black;margin-top: 37%;position: relative;margin-left: -20%;padding: 10px">
            <img src="<?=base_url()?>assets/gambar/pro_.png" style="width: 100%">
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel" style="width: 90%;opacity: 0.7; background-color: black;margin-top: 30%; color: white;position: relative;margin-left: -30%">
          <div class="panel panel-body" style="background-color: black;font-family: arial">
            <h3 style="font-weight: 900">Company Name</h3>
              <h4>&emsp;<?=$profil->company_name?></h4>
            <h3 style="font-weight: 900">Position</h3>
              <h4>&emsp;<?=$profil->posision?></h4>
            <h3 style="font-weight: 900">Phone Number</h3>
              <h4>&emsp;<?=$profil->phone_number?></h4>  
            <h3 style="font-weight: 900">Email</h3>
              <h4>&emsp;<?=$profil->email?></h4>
          </div>
        </div>
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-1 pull-right">
            <h1 style="margin-top: 450px ;transform: rotate(-90deg);font-weight: 800;font-size: 90px;opacity: 0.5">Profile.</h1>
      </div>
    </div>
</body>
</html>