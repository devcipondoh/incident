<!DOCTYPE html>
<html>
<head>
	<title>Search</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-image: url(<?=base_url('assets/gambar/bg_6.jpg')?>); background-size: cover">
    <div class="col-md-12" style="position: relative;z-index: 99999;margin-top: 0px;background-color: white;padding: 10px">
      <div class="container">
        <div class="pull-left">
          <a href="<?=base_url()?>Contribute/search" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200"><u><b>search.</b></u></a>&emsp;&emsp;
          <a href="<?=base_url()?>Contribute/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">contribute.</a>&emsp;&emsp;
          <a href="<?=base_url()?>About/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">about.</a>&emsp;&emsp;
          <a href="<?=base_url()?>Profile/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">profile.</a>&emsp;&emsp;
        </div>
        <div class="pull-right">
          <a href="<?=base_url()?>Paypal/"><button class="btn btn-xs" style=";margin-top: 5px;border-radius: 50px;color: white;background-color: black;font-size: 18px">&emsp;&emsp;&emsp;Join now&emsp;&emsp;&emsp;</button></a>
        </div>
      </div>
    </div>
	<div class="col-md-12" style="position: relative !important">
		<div class="container" style=";padding: 10px;opacity: 0.8">
			<h1 style="font-weight: bold;text-align: center;color: white"> ~ Result ~ </h1>
			<?php
			if (empty($result))
			{
			    echo '<h3 style="font-weight:900;color:white" align="center">No Data Found</h3>';
			}
			foreach ($result as $key => $value): ?>
			<table style="background-color: lightgrey" class="table table-striped col-md-6">
				<tr>
					<td width="300px">Name</td>
					<td width="5px">:</td>
					<td><?=$value->name_people?></td>
				</tr>
				<tr>
					<td>Passport or ID Card Number</td>
					<td>:</td>
					<td><?=$value->passport?></td>
				</tr>
				<tr>
					<td>Country of Passport or ID Card</td>
					<td>:</td>
					<td><?=$value->country_of_passport?></td>
				</tr>
				<tr>
					<td>Incident</td>
					<td>:</td>
					<td><?=$value->incident?></td>
				</tr>
				<tr>
					<td>chronology</td>
					<td>:</td>
					<td><?=$value->chronology?></td>
				</tr>
				<tr>
					<td>photos</td>
					<td>:</td>
					<td><?php
					// print_r(json_decode($value->foto,true));
					foreach (json_decode($value->foto,true) as $key => $foto) { ?>
						<img style="border: 2px solid white" src="<?=base_url().'assets/gambar/'.$foto['dok']?>" width="250px" height="250px">
					<?php }
					?></td>
				</tr>
			</table>
				<?php endforeach ?>
		</div>
	</div>
		

	
</body>
</html>