<!DOCTYPE html>
<html>
<head>
	<title>Search</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-image: url(<?=base_url('assets/gambar/bg_6.jpg')?>); background-size: cover">
	<div class="content">
    <div class="col-md-12" style="position: absolute;z-index: 99999;margin-top: -20px;background-color: white;padding: 10px">
      <div class="container">
        <div class="pull-left">
          <a href="<?=base_url()?>Search" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200"><u><b>search.</b></u></a>&emsp;&emsp;
          <a href="<?=base_url()?>Contribute/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">contribute.</a>&emsp;&emsp;
          <a href="<?=base_url()?>About/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">about.</a>&emsp;&emsp;
          <a href="<?=base_url()?>Profile/" style="color: black;font-size: 30px;font-family: times new roman;font-weight: 200">profile.</a>&emsp;&emsp;
        </div>
        <div class="pull-right">
          <a href="<?=base_url()?>Paypal/"><button class="btn btn-xs" style=";margin-top: 5px;border-radius: 50px;color: white;background-color: black;font-size: 18px">&emsp;&emsp;&emsp;Join now&emsp;&emsp;&emsp;</button></a>
        </div>
      </div>
    </div>
	<div class="content">
				<h1 style="color: white;opacity: 0.2;font-family: times new roman; font-size: 90px;position: absolute;transform: rotate(-90deg); margin-top: 400px;margin-left: -6%">Search.</h1>
		<h2 align="center" style="font-weight: 700;color: white;font-size: 55px"><br>What are you looking for?</h2><br><br>
		  <form style="margin-left: 5%;" class="col-md-5" method="POST" action="<?=base_url('Search/result')?>">
		    <h2 style="font-family: arial;font-weight: 900;color: white">&nbsp; You can find it.</h2><br>
		    <center>
	    		<div class="col-md-10">
	    			<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="name_people" placeholder="Name Of people"><br>
	    		</div>
	    		
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="incident" placeholder="Type Of Incident"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="passport" placeholder="Pasport or ID Card Number"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 40px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" name="country" placeholder="Country Of Origin"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="hidden" required name="detail" value="detail">
		    		<button class="btn" type="submit" style="color: black;background-color: lightgrey;font-size: 19px">&emsp;&emsp;Search&emsp;&emsp;</button><br><br><br>
		    	</div></center>
		    </form>
		    <div class="col-md-1"><br><br><br><br><br><br><br>
		    	<h3 class="text-center" style="color: white"><b>OR&emsp;&emsp;</b></h3>
		    </div>
		  <form style="margin-left: 3%;" class="col-md-5" action="<?=base_url('Search/result')?>" method="POST">
		  	<h2 style="font-family: arial;font-weight: 900;color: white">Just type the words here</h2><br>
		  	<input type="hidden" required name="word" value="word">
		  	<textarea class="form-control" name="textarea" placeholder="Type Here" style="height: 220px;font-weight: 600;text-align: center" required></textarea><br>
		    <center><button type="submit" class="btn" style="color: black;background-color: lightgrey;font-size: 19px">&emsp;&emsp;Search&emsp;&emsp;</button><br><br><br></center>
		  </form>
		</div>
	</div>
    <?=$this->session->flashdata('msg')?>
</body>
</html>