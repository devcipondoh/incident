<!DOCTYPE html>
<html>
<head>
	<title>Sign up</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="color: black;font-family: arial;opacity: 0.9">
	<a href="<?=base_url()?>Page" style="color:black;position: absolute;margin-top: -2.5em;padding: 10px;font-size: 25px;opacity: 0.8;text-decoration: none"><i class="glyphicon glyphicon-arrow-left"></i>&emsp;<b>Back</b></a>
	<h1 style="opacity: 0.1;font-family: times new roman; font-size: 90px;position: absolute;transform: rotate(270deg); margin-top: 350px;margin-left: -100px">Sign up.</h1>
	<div class="conteiner" style="margin-top: 5em">
		<center>
		    <h3 class="text-danger"><?=$this->session->flashdata('msg')?></h3>
		<div class="panel" style="width: 50%">
		  <div class="panel-body" style="box-shadow: 0px 1px 3px 1px grey;">
		    <h2 style="font-family: times new roman;font-weight: 900">sign up.</h2><br>
		    <form style="margin-left: 15%;" method="POST" action="<?=base_url('Page/save_sign_up')?>">
	    		<div class="col-md-5">
	    			<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="first_name" placeholder="First Name">
	    		</div>
	    		<div class="col-md-5">
	    			<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" placeholder="Surname" required name="surname"><br>
	    		</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="company_name" placeholder="Company Name"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="position" placeholder="Position"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="phone_number" placeholder="Phone Number"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="email" placeholder="Email"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="text" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="username" placeholder="Username"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="password" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="password" placeholder="Password"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<input type="password" style="border:solid; height: 35px; text-align: center;font-weight: 700; font-size: 19px" class="form-control" required name="re_password" placeholder="Re-Password"><br>
		    	</div>
		    	<div class="col-md-10">
		    		<button class="btn btn-warning" type="submit" style="color: white;background-color: black;font-size: 19px">&emsp;&emsp;Submit&emsp;&emsp;</button><br><br><br>
		    	</div>
		    </form>
		  </div>
		</div>
	</center>
	</div>
</body>
</html>