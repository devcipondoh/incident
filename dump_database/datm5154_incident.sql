-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 06, 2019 at 10:23 PM
-- Server version: 10.2.21-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datm5154_incident`
--

-- --------------------------------------------------------

--
-- Table structure for table `id_type`
--

CREATE TABLE `id_type` (
  `id` int(11) NOT NULL,
  `nama_id_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_type`
--

INSERT INTO `id_type` (`id`, `nama_id_type`) VALUES
(1, 'PASSPORT'),
(2, 'ID CARD');

-- --------------------------------------------------------

--
-- Table structure for table `negara`
--

CREATE TABLE `negara` (
  `nama_negara` varchar(255) DEFAULT NULL,
  `ibukota` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `negara`
--

INSERT INTO `negara` (`nama_negara`, `ibukota`) VALUES
('Afghanistan ', 'ASIA (EX. NEAR EAST) '),
('Albania ', 'EASTERN EUROPE '),
('Algeria ', 'NORTHERN AFRICA '),
('American Samoa ', 'OCEANIA '),
('Andorra ', 'WESTERN EUROPE '),
('Angola ', 'SUB-SAHARAN AFRICA '),
('Anguilla ', 'LATIN AMER. & CARIB '),
('Antigua & Barbuda ', 'LATIN AMER. & CARIB '),
('Argentina ', 'LATIN AMER. & CARIB '),
('Armenia ', 'C.W. OF IND. STATES '),
('Aruba ', 'LATIN AMER. & CARIB '),
('Australia ', 'OCEANIA '),
('Austria ', 'WESTERN EUROPE '),
('Azerbaijan ', 'C.W. OF IND. STATES '),
('Bahamas, The ', 'LATIN AMER. & CARIB '),
('Bahrain ', 'NEAR EAST '),
('Bangladesh ', 'ASIA (EX. NEAR EAST) '),
('Barbados ', 'LATIN AMER. & CARIB '),
('Belarus ', 'C.W. OF IND. STATES '),
('Belgium ', 'WESTERN EUROPE '),
('Belize ', 'LATIN AMER. & CARIB '),
('Benin ', 'SUB-SAHARAN AFRICA '),
('Bermuda ', 'NORTHERN AMERICA '),
('Bhutan ', 'ASIA (EX. NEAR EAST) '),
('Bolivia ', 'LATIN AMER. & CARIB '),
('Bosnia & Herzegovina ', 'EASTERN EUROPE '),
('Botswana ', 'SUB-SAHARAN AFRICA '),
('Brazil ', 'LATIN AMER. & CARIB '),
('British Virgin Is. ', 'LATIN AMER. & CARIB '),
('Brunei ', 'ASIA (EX. NEAR EAST) '),
('Bulgaria ', 'EASTERN EUROPE '),
('Burkina Faso ', 'SUB-SAHARAN AFRICA '),
('Burma ', 'ASIA (EX. NEAR EAST) '),
('Burundi ', 'SUB-SAHARAN AFRICA '),
('Cambodia ', 'ASIA (EX. NEAR EAST) '),
('Cameroon ', 'SUB-SAHARAN AFRICA '),
('Canada ', 'NORTHERN AMERICA '),
('Cape Verde ', 'SUB-SAHARAN AFRICA '),
('Cayman Islands ', 'LATIN AMER. & CARIB '),
('Central African Rep. ', 'SUB-SAHARAN AFRICA '),
('Chad ', 'SUB-SAHARAN AFRICA '),
('Chile ', 'LATIN AMER. & CARIB '),
('China ', 'ASIA (EX. NEAR EAST) '),
('Colombia ', 'LATIN AMER. & CARIB '),
('Comoros ', 'SUB-SAHARAN AFRICA '),
('Congo, Dem. Rep. ', 'SUB-SAHARAN AFRICA '),
('Congo, Repub. of the ', 'SUB-SAHARAN AFRICA '),
('Cook Islands ', 'OCEANIA '),
('Costa Rica ', 'LATIN AMER. & CARIB '),
('Cote d\'Ivoire ', 'SUB-SAHARAN AFRICA '),
('Croatia ', 'EASTERN EUROPE '),
('Cuba ', 'LATIN AMER. & CARIB '),
('Cyprus ', 'NEAR EAST '),
('Czech Republic ', 'EASTERN EUROPE '),
('Denmark ', 'WESTERN EUROPE '),
('Djibouti ', 'SUB-SAHARAN AFRICA '),
('Dominica ', 'LATIN AMER. & CARIB '),
('Dominican Republic ', 'LATIN AMER. & CARIB '),
('East Timor ', 'ASIA (EX. NEAR EAST) '),
('Ecuador ', 'LATIN AMER. & CARIB '),
('Egypt ', 'NORTHERN AFRICA '),
('El Salvador ', 'LATIN AMER. & CARIB '),
('Equatorial Guinea ', 'SUB-SAHARAN AFRICA '),
('Eritrea ', 'SUB-SAHARAN AFRICA '),
('Estonia ', 'BALTICS '),
('Ethiopia ', 'SUB-SAHARAN AFRICA '),
('Faroe Islands ', 'WESTERN EUROPE '),
('Fiji ', 'OCEANIA '),
('Finland ', 'WESTERN EUROPE '),
('France ', 'WESTERN EUROPE '),
('French Guiana ', 'LATIN AMER. & CARIB '),
('French Polynesia ', 'OCEANIA '),
('Gabon ', 'SUB-SAHARAN AFRICA '),
('Gambia, The ', 'SUB-SAHARAN AFRICA '),
('Gaza Strip ', 'NEAR EAST '),
('Georgia ', 'C.W. OF IND. STATES '),
('Germany ', 'WESTERN EUROPE '),
('Ghana ', 'SUB-SAHARAN AFRICA '),
('Gibraltar ', 'WESTERN EUROPE '),
('Greece ', 'WESTERN EUROPE '),
('Greenland ', 'NORTHERN AMERICA '),
('Grenada ', 'LATIN AMER. & CARIB '),
('Guadeloupe ', 'LATIN AMER. & CARIB '),
('Guam ', 'OCEANIA '),
('Guatemala ', 'LATIN AMER. & CARIB '),
('Guernsey ', 'WESTERN EUROPE '),
('Guinea ', 'SUB-SAHARAN AFRICA '),
('Guinea-Bissau ', 'SUB-SAHARAN AFRICA '),
('Guyana ', 'LATIN AMER. & CARIB '),
('Haiti ', 'LATIN AMER. & CARIB '),
('Honduras ', 'LATIN AMER. & CARIB '),
('Hong Kong ', 'ASIA (EX. NEAR EAST) '),
('Hungary ', 'EASTERN EUROPE '),
('Iceland ', 'WESTERN EUROPE '),
('India ', 'ASIA (EX. NEAR EAST) '),
('Indonesia ', 'ASIA (EX. NEAR EAST) '),
('Iran ', 'ASIA (EX. NEAR EAST) '),
('Iraq ', 'NEAR EAST '),
('Ireland ', 'WESTERN EUROPE '),
('Isle of Man ', 'WESTERN EUROPE '),
('Israel ', 'NEAR EAST '),
('Italy ', 'WESTERN EUROPE '),
('Jamaica ', 'LATIN AMER. & CARIB '),
('Japan ', 'ASIA (EX. NEAR EAST) '),
('Jersey ', 'WESTERN EUROPE '),
('Jordan ', 'NEAR EAST '),
('Kazakhstan ', 'C.W. OF IND. STATES '),
('Kenya ', 'SUB-SAHARAN AFRICA '),
('Kiribati ', 'OCEANIA '),
('Korea, North ', 'ASIA (EX. NEAR EAST) '),
('Korea, South ', 'ASIA (EX. NEAR EAST) '),
('Kuwait ', 'NEAR EAST '),
('Kyrgyzstan ', 'C.W. OF IND. STATES '),
('Laos ', 'ASIA (EX. NEAR EAST) '),
('Latvia ', 'BALTICS '),
('Lebanon ', 'NEAR EAST '),
('Lesotho ', 'SUB-SAHARAN AFRICA '),
('Liberia ', 'SUB-SAHARAN AFRICA '),
('Libya ', 'NORTHERN AFRICA '),
('Liechtenstein ', 'WESTERN EUROPE '),
('Lithuania ', 'BALTICS '),
('Luxembourg ', 'WESTERN EUROPE '),
('Macau ', 'ASIA (EX. NEAR EAST) '),
('Macedonia ', 'EASTERN EUROPE '),
('Madagascar ', 'SUB-SAHARAN AFRICA '),
('Malawi ', 'SUB-SAHARAN AFRICA '),
('Malaysia ', 'ASIA (EX. NEAR EAST) '),
('Maldives ', 'ASIA (EX. NEAR EAST) '),
('Mali ', 'SUB-SAHARAN AFRICA '),
('Malta ', 'WESTERN EUROPE '),
('Marshall Islands ', 'OCEANIA '),
('Martinique ', 'LATIN AMER. & CARIB '),
('Mauritania ', 'SUB-SAHARAN AFRICA '),
('Mauritius ', 'SUB-SAHARAN AFRICA '),
('Mayotte ', 'SUB-SAHARAN AFRICA '),
('Mexico ', 'LATIN AMER. & CARIB '),
('Micronesia, Fed. St. ', 'OCEANIA '),
('Moldova ', 'C.W. OF IND. STATES '),
('Monaco ', 'WESTERN EUROPE '),
('Mongolia ', 'ASIA (EX. NEAR EAST) '),
('Montserrat ', 'LATIN AMER. & CARIB '),
('Morocco ', 'NORTHERN AFRICA '),
('Mozambique ', 'SUB-SAHARAN AFRICA '),
('Namibia ', 'SUB-SAHARAN AFRICA '),
('Nauru ', 'OCEANIA '),
('Nepal ', 'ASIA (EX. NEAR EAST) '),
('Netherlands ', 'WESTERN EUROPE '),
('Netherlands Antilles ', 'LATIN AMER. & CARIB '),
('New Caledonia ', 'OCEANIA '),
('New Zealand ', 'OCEANIA '),
('Nicaragua ', 'LATIN AMER. & CARIB '),
('Niger ', 'SUB-SAHARAN AFRICA '),
('Nigeria ', 'SUB-SAHARAN AFRICA '),
('N. Mariana Islands ', 'OCEANIA '),
('Norway ', 'WESTERN EUROPE '),
('Oman ', 'NEAR EAST '),
('Pakistan ', 'ASIA (EX. NEAR EAST) '),
('Palau ', 'OCEANIA '),
('Panama ', 'LATIN AMER. & CARIB '),
('Papua New Guinea ', 'OCEANIA '),
('Paraguay ', 'LATIN AMER. & CARIB '),
('Peru ', 'LATIN AMER. & CARIB '),
('Philippines ', 'ASIA (EX. NEAR EAST) '),
('Poland ', 'EASTERN EUROPE '),
('Portugal ', 'WESTERN EUROPE '),
('Puerto Rico ', 'LATIN AMER. & CARIB '),
('Qatar ', 'NEAR EAST '),
('Reunion ', 'SUB-SAHARAN AFRICA '),
('Romania ', 'EASTERN EUROPE '),
('Russia ', 'C.W. OF IND. STATES '),
('Rwanda ', 'SUB-SAHARAN AFRICA '),
('Saint Helena ', 'SUB-SAHARAN AFRICA '),
('Saint Kitts & Nevis ', 'LATIN AMER. & CARIB '),
('Saint Lucia ', 'LATIN AMER. & CARIB '),
('St Pierre & Miquelon ', 'NORTHERN AMERICA '),
('Saint Vincent and the Grenadines ', 'LATIN AMER. & CARIB '),
('Samoa ', 'OCEANIA '),
('San Marino ', 'WESTERN EUROPE '),
('Sao Tome & Principe ', 'SUB-SAHARAN AFRICA '),
('Saudi Arabia ', 'NEAR EAST '),
('Senegal ', 'SUB-SAHARAN AFRICA '),
('Serbia ', 'EASTERN EUROPE '),
('Seychelles ', 'SUB-SAHARAN AFRICA '),
('Sierra Leone ', 'SUB-SAHARAN AFRICA '),
('Singapore ', 'ASIA (EX. NEAR EAST) '),
('Slovakia ', 'EASTERN EUROPE '),
('Slovenia ', 'EASTERN EUROPE '),
('Solomon Islands ', 'OCEANIA '),
('Somalia ', 'SUB-SAHARAN AFRICA '),
('South Africa ', 'SUB-SAHARAN AFRICA '),
('Spain ', 'WESTERN EUROPE '),
('Sri Lanka ', 'ASIA (EX. NEAR EAST) '),
('Sudan ', 'SUB-SAHARAN AFRICA '),
('Suriname ', 'LATIN AMER. & CARIB '),
('Swaziland ', 'SUB-SAHARAN AFRICA '),
('Sweden ', 'WESTERN EUROPE '),
('Switzerland ', 'WESTERN EUROPE '),
('Syria ', 'NEAR EAST '),
('Taiwan ', 'ASIA (EX. NEAR EAST) '),
('Tajikistan ', 'C.W. OF IND. STATES '),
('Tanzania ', 'SUB-SAHARAN AFRICA '),
('Thailand ', 'ASIA (EX. NEAR EAST) '),
('Togo ', 'SUB-SAHARAN AFRICA '),
('Tonga ', 'OCEANIA '),
('Trinidad & Tobago ', 'LATIN AMER. & CARIB '),
('Tunisia ', 'NORTHERN AFRICA '),
('Turkey ', 'NEAR EAST '),
('Turkmenistan ', 'C.W. OF IND. STATES '),
('Turks & Caicos Is ', 'LATIN AMER. & CARIB '),
('Tuvalu ', 'OCEANIA '),
('Uganda ', 'SUB-SAHARAN AFRICA '),
('Ukraine ', 'C.W. OF IND. STATES '),
('United Arab Emirates ', 'NEAR EAST '),
('United Kingdom ', 'WESTERN EUROPE '),
('United States ', 'NORTHERN AMERICA '),
('Uruguay ', 'LATIN AMER. & CARIB '),
('Uzbekistan ', 'C.W. OF IND. STATES '),
('Vanuatu ', 'OCEANIA '),
('Venezuela ', 'LATIN AMER. & CARIB '),
('Vietnam ', 'ASIA (EX. NEAR EAST) '),
('Virgin Islands ', 'LATIN AMER. & CARIB '),
('Wallis and Futuna ', 'OCEANIA '),
('West Bank ', 'NEAR EAST '),
('Western Sahara ', 'NORTHERN AFRICA '),
('Yemen ', 'NEAR EAST '),
('Zambia ', 'SUB-SAHARAN AFRICA '),
('Zimbabwe ', 'SUB-SAHARAN AFRICA ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contribute`
--

CREATE TABLE `tbl_contribute` (
  `id_contribute` int(11) NOT NULL,
  `name_people` text NOT NULL,
  `id_type` varchar(255) NOT NULL,
  `passport` varchar(255) NOT NULL,
  `country_of_passport` varchar(255) NOT NULL,
  `chronology` text NOT NULL,
  `incident` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contribute`
--

INSERT INTO `tbl_contribute` (`id_contribute`, `name_people`, `id_type`, `passport`, `country_of_passport`, `chronology`, `incident`, `foto`, `created_at`) VALUES
(3, 'dimas', '11', '11', '11', 'nsdkjnfskjn', '11', '[{\"dok\":\"profil1.png\"}]', '2018-11-27 23:50:41'),
(4, 'nanda', '1', '12345', 'indonesia', 'a fight between him and the waiters ', 'fight', '[{\"dok\":\"foto_funedge.png\"}]', '2018-11-29 22:56:43'),
(5, 'sndk', 'PASSPORT', 'kjhjk', 'Indonesia ', 'nlkkl', 'jkhkjhkjh', '[]', '2018-12-02 23:15:22'),
(6, 'kasnmdlk', 'knslkns', 'nksn', 'nclks', 'ankln', 'nkn', '[{\"dok\":\"kuesioner.jpg\"}]', '2018-12-03 00:09:58'),
(7, 'huma', 'PASSPORT', '99887766', 'Uganda ', 'drunk and start a fight with other people', 'fight', '[{\"dok\":\"ROCKR_200-300x375.jpg\"}]', '2018-12-04 21:32:52'),
(8, 'Brendan Butterworh ', 'PASSPORT', 'LK745286', 'New Zealand ', 'This morning I caught Brendan sleeping in a room that was not his. When I asked him to pay for the room he starting abusing the staff - called us all the c word & made a huge threats Please call the police if he arrives!', 'abusive/ free loader/ sneaking ', '[{\"dok\":\"Butterworth.jpg\"}]', '2018-12-16 10:30:13'),
(9, 'Kristy Brown', 'PASSPORT', 'N5479889', 'Australia ', 'Using drugs on Premises, gets locked up during stay and does not remove her things.', 'Drug Use', '[]', '2019-01-08 15:57:06'),
(10, 'Kristy Brown', 'PASSPORT', 'N5479889', 'Australia ', 'Using drugs on Premises, gets locked up during stay and does not remove her things.', 'Drug Use', '[]', '2019-01-08 15:57:08'),
(11, 'Benjamin Davis Johnson', 'PASSPORT', 'Unknown', 'Australia ', 'See trip advisor reviews.', 'Continual Drunk. Causes mayhem.', '[{\"dok\":\"2019-01-08_17_05_42-WhatsApp.png\"}]', '2019-01-08 16:06:43'),
(12, '', '', '', '', '', '', '[]', '2019-01-11 13:51:36'),
(13, '', '', '', '', '', '', '[]', '2019-01-11 13:51:39'),
(14, 'Joshua Whitley', 'PASSPORT', 'N2571804', 'Australia ', 'he was using threatening behavior with other guests to get alcohol. Tobacco went missing in his room but couldn\\\'t prove it was him, but most likely. And now we have him on camera returning to the hive after he had checked out to steal alcohol off the guest shelves. Also a guest showed us a facebook page where french guests of Billabong put up their own warning about this guy.', 'threatening behavior , Stealing stuff', '[]', '2019-01-11 13:56:39'),
(15, 'Connel Gerard Byrne', 'PASSPORT', 'N8650102', 'Australia ', 'Guest gets violent when intoxicated.', 'Alcohol Violence', '[]', '2019-01-30 09:48:03'),
(16, 'Niran Osswald', 'PASSPORT', 'C4N1R4ZWGD', 'Germany ', 'Going to Female toilets', 'Sneaking into Female Toilets', '[]', '2019-02-06 08:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sign_up`
--

CREATE TABLE `tbl_sign_up` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `posision` varchar(255) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `subscribe` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `activate` int(1) NOT NULL DEFAULT 0,
  `md5` varchar(255) NOT NULL,
  `expired` datetime(6) NOT NULL,
  `invoice_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sign_up`
--

INSERT INTO `tbl_sign_up` (`username`, `password`, `first_name`, `surname`, `company_name`, `posision`, `phone_number`, `email`, `create_at`, `subscribe`, `hash`, `activate`, `md5`, `expired`, `invoice_id`) VALUES
('ajos', '8d1dc4933adb29754cac716880cc900b', 'Ajos', 'King', 'Heroes', 'SEO', '+6287870522999', 'ajosking@gmail.com', '2018-12-02 10:27:45', 1, '', 1, '', '0000-00-00 00:00:00.000000', 0),
('dimas_11', '8613985ec49eb8f757ae6439e879bb2a', 'dimas', 'Kumara', 'Dimas\\\'s chocolate', 'Manager', '021', 'dimaskum11@gmail.com', '2018-11-29 00:46:22', 0, 'Lo1535re2369m38613985ec49eb8f757ae6439e879bb2a', 1, '84ca45e5c8de7f34c29b2b522a824101', '0000-00-00 00:00:00.000000', 0),
('snkjdns', '8277e0910d750195b448797616e091ad', 'sdjnbskjn', 'nklndkls', 'snldsn', 'lsndl', '098', 'ggg@gmail.com', '2018-12-02 10:00:44', 0, '763785b2f38cc576ebeb63df34c666d3f1d0a7', 0, '2636e746c8b41ab82dc206080d8300f3', '0000-00-00 00:00:00.000000', 0),
('yohan', 'e19f883b413cf3a1aae5bbcc9995dd9c', 'yohan', 'yohan', 'funedge', 'director', '0818737573', 'singkole@gmail.com', '2018-11-29 15:28:14', 0, '12766922c22895822830f4f128cd72863c08a2', 1, '17500a7e3f7eaed02e5745c9ba0e2ad2', '0000-00-00 00:00:00.000000', 0),
('yohan3', 'e19f883b413cf3a1aae5bbcc9995dd9c', 'john', 'john', 'funedge', 'director', '+62818737573', 'yohantw@yahoo.com', '2018-12-02 14:00:02', 0, '396637527bd5b5d689e2c32ae974c6229ff785', 0, 'b8375f735395b562ed6633a4f210bfbd', '0000-00-00 00:00:00.000000', 0),
('yohantw', 'e19f883b413cf3a1aae5bbcc9995dd9c', 'yohan', 'walandouw', 'funco', 'sales', '+618182222222', 'ngopitubrukanget@gmail.com', '2018-11-28 21:08:51', 0, '', 0, '', '0000-00-00 00:00:00.000000', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `id_type`
--
ALTER TABLE `id_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contribute`
--
ALTER TABLE `tbl_contribute`
  ADD PRIMARY KEY (`id_contribute`);

--
-- Indexes for table `tbl_sign_up`
--
ALTER TABLE `tbl_sign_up`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `id_type`
--
ALTER TABLE `id_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_contribute`
--
ALTER TABLE `tbl_contribute`
  MODIFY `id_contribute` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
